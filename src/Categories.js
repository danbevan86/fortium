import React, { Component } from "react";
 
class Categories extends Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            categories: []
        };
    }

    componentDidMount() {
        fetch("https://api.publicapis.org/categories" )
        .then(res => res.json())
        .then(
            (result) => {
                this.setState({
                    isLoaded: true,
                    categories: result
                });
            },
            (error) => {
                this.setState({
                    isLoaded: true,
                    error
                });
            }
        )
    }

    render() {
        const { error, isLoaded, categories } = this.state;
        if(error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Loading...</div>;
        } else {
            return (
                <div className="categories-collection">
                    <h3 className="ui block center aligned header">
                        Categories
                    </h3>

                    <p>There are {categories.length} categories listed below.</p>
                    <table className="ui fixed celled stripe table">
                        <thead>
                            <tr>
                            <th>Category</th>
                            </tr>
                        </thead>
                        <tbody>
                            {categories.map(category => (
                                <tr key={category}>
                                    <td>{category}</td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            );
        }
    }
}
 
export default Categories;