import React, { Component } from "react";
 
class Entries extends Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            entries: [],
            results: []
        };
    }

    componentDidMount() {
        fetch("https://api.publicapis.org/entries?https=true")
        .then(res => res.json())

        .then(
            (result) => {
                this.setState({
                    isLoaded: true,
                    entries: result.entries.map((entry, index) => {
                        return(
                            <tr key={index}>
                                <td className="collapsing">{entry.API}</td>
                                <td>{entry.Description}</td>
                                <td className="collapsing"><a target="_blank" rel="noopener noreferrer" href={entry.Link}>{entry.Link}</a></td>
                                <td className="collapsing">{entry.Category}</td>
                            </tr>
                        )
                    }),
                    results: result
                });
            },
            (error) => {
                this.setState({
                    isLoaded: true,
                    error
                });
            }
        )
    }

    render() {
        const { error, isLoaded, entries} = this.state;
        if(error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Loading...</div>;
        } else {
            return (
                <div className="entries-collection">
                    <h3 className="ui block center aligned header">
                        Entries
                    </h3>
                    <p>There are {entries.length} entries listed below.</p>

                    <table className="ui fixed celled stripe table">
                        <thead>
                            <tr>
                            <th>Entry</th>
                            <th>Description</th>
                            <th>Link</th>
                            <th>Category</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.entries}
                        </tbody>
                    </table>
                </div>
            );
        }
    }
}
 
export default Entries;