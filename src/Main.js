import React, { Component } from "react";
import {
    Route,
    NavLink,
    HashRouter
  } from "react-router-dom";
  import Entries from "./Entries";
  import Categories from "./Categories";
  import "../node_modules/semantic-ui-css/semantic.min.css";
  import "./style.css";

class Main extends Component {
  render() {
    return (
        <HashRouter>
        <div className="ui container">
            <div className="ui grid">
                <div className="four wide column">
                    <div className="ui vertical fluid menu">
                        <div className="header item">
                            <p>Fortium</p> <i class="globe icon"></i>
                        </div>
                        <NavLink to="/entries" className="item"><i class="file outline icon"></i><p>Entries</p></NavLink>
                        <NavLink to="/categories" className="item"><i class="file outline icon"></i><p>Categories</p></NavLink>
                    </div>
                </div>
                <div className="twelve wide stretched column">
                    <Route exact path="/" component={Entries}/>
                    <Route path="/entries" component={Entries}/>
                    <Route path="/categories" component={Categories}/>
                </div>
            </div>
        </div>
        </HashRouter>
    );
  }
}
 
export default Main;